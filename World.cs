﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using Project2.Models;
using Project2.Models.SA;
using Project2.Models.TS;

namespace Project2
{
    class World
    {
        public int[,] Matrix { get; set; }
        public int Size { get; set; } = 0;
        public int Time { get; set; } = 1;

        public void RunProject()
        {
            bool show = true;
            while (show)
            {
                show = MainMenu();
            }
        }

        private bool MainMenu()
        {
            Console.Clear();
            Console.WriteLine("Wybierz opcję: \n" +
                              "1 -> Załaduj Matrix \n" +
                              "2 -> Wyświetl informacje \n" +
                              "3 -> Kryterum stopu \n" +
                              "4 -> Algorytm SA\n" +
                              "5 -> Algorytm TS \n" +
                              "6 -> Wyjscie \n");
            int value = Int32.Parse(Console.ReadLine() ?? throw new InvalidOperationException());

            switch (value)
            {
                case 1:
                    FileRead();
                    break;
 
                case 2:
                    showMatrix();
                    break;

                case 3:
                    Console.WriteLine("Podaj kryterium stopu ");
                    Time = Int32.Parse(Console.ReadLine() ?? throw new InvalidOperationException());
                    break;

                case 4:
                    AlgorithmSA algorithmSA = new AlgorithmSA(Matrix, Size);
                    algorithmSA.Start();
                    break;
                case 5:
                    AlgorithmTS algorithmTS = new AlgorithmTS(Matrix, Size, Time);
                    algorithmTS.Start();
                    break;

                case 6:
                    return false;
                default:
                    break;
            }

            return true;
        }


        public void FileRead()
        {
            Console.WriteLine("Podaj nazwę pliku");
            string fileName = Console.ReadLine();
            //string fileName = "t0";

            try
            {
                using (StreamReader stream = new StreamReader("../../Database/" + fileName + ".txt"))
                {
                    Console.WriteLine("Wczytywanie danych...");
                    List<string> list = new List<string>();
                    String line = stream.ReadLine();

                    if (line != null)
                    {
                        Size = Int32.Parse(line);
                        line = stream.ReadLine();
                        while (line != null)
                        {
                            string[] infoStrings = Regex.Split(line, @"\D+");
                            list.AddRange(infoStrings);
                            line = stream.ReadLine();
                        }

                        int x = 0;
                        int y = 0;
                        Matrix = new int[Size, Size];

                        for (int i = 0; i < list.Count; i++)                      
                        {
                            if(list[i] != "")
                            {
                                Matrix[x, y] = Int32.Parse(list[i]);
                                //Console.WriteLine(x + ", " + y + " : " + list[i]);
                                y++;
                                if (y == Size)
                                {
                                    y = 0;
                                    x++;
                                }
                            }
                        }
                    }

                    Console.WriteLine("...Wczytywanie zakończone.");
                    showMatrix();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Nie można otworzyć pliku");
                Console.WriteLine(e.Message);
                Console.ReadKey();
            }
        }

        public void showMatrix()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j=0; j<Size; j++)
                {
                    Console.Write(Matrix[i, j] + " ");
                }
                Console.WriteLine();
            }

            Console.WriteLine("Kryterium stopu: " + Time + "s");

            Console.ReadKey();

        }
    }
}