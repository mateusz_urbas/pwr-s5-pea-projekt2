﻿
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Models.SA
{
    class AlgorithmSA
    {
        private int[,] Matrix { get; set; }
        private int Size { get; set; }
        private bool Test { get; set; }

        private double Temperature { get; set; }

        private int[] Solution { get; set; }


        public int[] BestSolution { get; set; }
        public int BestSolutionValue { get; set; }

        Random random = new Random();
        Stopwatch stopwatch = new Stopwatch();


        public AlgorithmSA(int[,] matrix, int size, bool test = false)
        {
            Size = size;
            Test = test;
            initMatrix(matrix);

            Temperature = Size / Math.Log10(Size);
            Solution = new int[Size + 1];
            BestSolution = new int[Size + 1];
        }

        private void initMatrix(int[,] matrix)
        {
            Matrix = new int[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Matrix[i, j] = matrix[i, j];
                }
            }
        }

        public void Start()
        {
            stopwatch.Reset();
            stopwatch.Start();
            //poczatkowe randomowe rozwiazanie
            InitRandomSolution();
            int i = 1;
            while (Temperature > 2)
            {
                Temperature = G(i);
                Next();
                i++;
            }
            stopwatch.Stop();

            if (!Test)
            {
                Console.WriteLine("czas: " + stopwatch.Elapsed.TotalSeconds);
                Console.WriteLine("obrotow: " + i);
                if(BestSolutionValue<getValue(Solution))
                {
                    ShowResult(BestSolution);
                }
                else
                {
                    ShowResult(Solution);
                }
            }
        }

        // funkcja obnizania temp
        private double G(int i)
        {
            return  Temperature * 0.9999991;
        }

        private void Next()
        {
            int[] newSolution = new int[Size + 1];

            generateSolution(newSolution);

            int solutionValue = getValue(Solution);
            int newSolutionValue = getValue(newSolution);

            // zapis najlepszego rozwiazania dla bezpieczenstwa
            if(newSolutionValue < BestSolutionValue)
            {
                SaveBestSolution(newSolution);
            }

            int value = newSolutionValue - solutionValue;
            if (value < 0)
            {
                AcceptSolution(newSolution);
            }
            else
            {
                if(random.NextDouble() < P(value))
                {
                    AcceptSolution(newSolution);
                }
            }
        }

        private void generateSolution(int[] newSolution)
        {
            List<int> list = new List<int>();

            list.AddRange(Solution);

            int from, to;

            do
            {
                from = random.Next(1, Size - 1);
                to = random.Next(1, Size - 1);
            } while (from == to);

            int element = list[from]; 
            list.RemoveAt(from);
            list.Insert(to, element);

            for (int i = 0; i < list.Count; i++)
            {
                newSolution[i] = list[i];
            }

        }

        private void AcceptSolution(int[] newSolution)
        {
            for (int i = 1; i < Size; i++)
            {
                Solution[i] = newSolution[i];
            }
        }

        private void SaveBestSolution(int[] solution)
        {
            for (int i = 1; i < Size; i++)
            {
                BestSolution[i] = solution[i];
            }

            BestSolutionValue = getValue(BestSolution);
        }

        // funkcja prawdopodobienstwa
        private double P(int value)
        {
            //stała eulera do potęgi różnicy podzielonej przez temp
            //im wieksza różnica tym mniejsze prawdopodobieństwo
            //im mniejsza temperatura tym mniejsze prawdopodobienstwo
            return Math.Exp(-(value / Temperature)); 
        }

        private int getValue(int[] solution)
        {
            int result = 0;
            for (int i = 0; i < Size; i++)
            {
                result += Matrix[solution[i], solution[i + 1]];
            }
            return result;
        }

        private void InitRandomSolution()
        {
            List<int> list = new List<int>(); 
            for (int i = 1; i < Size; i++)
            {
                list.Add(i);
            }
    
            Solution[0] = 0;
            Solution[Size] = 0;

            for (int i = 1 ; i < Size; i++)
            {
                int selected = random.Next(0, list.Count);
                int selectedValue = list[selected];

                Solution[i] = selectedValue;
                list.Remove(selectedValue);
            }

            SaveBestSolution(Solution);
        }

        public void ShowResult(int[] solution)
        {
            Console.WriteLine("Rezultat: \n");
            for (int i = 0; i < Size; i++)
            {
                Console.Write(solution[i] + " --> ");
            }
            Console.Write(0);

            Console.WriteLine("\nKoszt: " + getValue(solution));
            Console.ReadKey();
        }
    }
}
