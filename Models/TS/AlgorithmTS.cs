﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project2.Models.TS
{
    class AlgorithmTS
    {
        private int[,] Matrix { get; set; }
        private int Size { get; set; }
        private bool Test { get; set; }
        private int Time { get; set; }

        public int TabuValue { get; set; } = 10;
        private int[,] Tabu { get; set; }   
        private int[] Solution { get; set; }
        private int Value { get; set; }

        public int[] BestSolution { get; set; }
        public int BestSolutionValue { get; set; }

        Random random = new Random();
        Stopwatch stopwatch = new Stopwatch();


        public AlgorithmTS(int[,] matrix, int size, int time, bool test = false)
        {
            Size = size;
            Time = time;
            Test = test;

            initMatrix(matrix);
            Solution = new int[Size + 1];
            BestSolution = new int[Size + 1];
            BestSolutionValue = Int32.MaxValue;
            initTabu();
        }

        private void initTabu()
        {
            Tabu = new int[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Tabu[i, j] = 0;
                }
            }
        }

        private void initMatrix(int[,] matrix)
        {
            Matrix = new int[Size, Size];
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    Matrix[i, j] = matrix[i, j];
                }
            }
        }

        public void Start()
        {
            InitRandomSolution();
            stopwatch.Reset();
            stopwatch.Start();
            while (Time > (stopwatch.Elapsed.Seconds) + (stopwatch.Elapsed.Minutes*60))
            {
                getBestNeighbor();
                Console.WriteLine(stopwatch.Elapsed.Seconds + (stopwatch.Elapsed.Minutes * 60));
            }
            stopwatch.Stop();


            if (!Test)
            {
                if(Value < BestSolutionValue)
                {
                    ShowResult(Solution);
                }
                else
                {
                    ShowResult(BestSolution);
                }
            }
        }

        private void getBestNeighbor()
        {
            Neighbor bestNeighbor = new Neighbor(Size); 
            int[] newSolution = new int[Size+1];

            int number1, number2;

            // znalezienie obszaru przeszukiwania sąsiada
            //int center = random.Next(1, Size - 1);
            //int rangeFrom = (center - 10) > 0 ? center - 10 : 1;
            //int rangeTo = (center + 10) < Size-1 ? (center + 10) : (Size - 1);

            int rangeFrom = 1;
            int rangeTo = Size - 1;

            int created = 0;
            // szukanie 10 sąsiadów i wybór najlepszego
            for (int i = 0; i < Size; i++)
            {
                do
                {
                    number1 = random.Next(rangeFrom, rangeTo);
                    number2 = random.Next(rangeFrom, rangeTo);
                } while (number1 == number2);

                if(CheckInTabu(number1, number2))
                {
                    created++;
                    generateNewSolution(newSolution, number1, number2);

                    int newValue = getValue(newSolution);
                    if (newValue < bestNeighbor.Value)
                    {
                        bestNeighbor.ChangeBestNeighbor(number1, number2, newSolution, newValue);
                    }
                }
            }
            if(created>0)
            {
                // zapis najlepszego znalezionego rozwiazania dla bezpieczenstwa
                if (BestSolutionValue > Value)
                {
                    for (int j = 0; j < Size + 1; j++)
                    {
                        BestSolution[j] = Solution[j];
                    }
                    BestSolutionValue = Value;
                }

                // zapis najlepszej zamiany
                for (int j = 0; j < Size + 1; j++)
                {
                    Solution[j] = bestNeighbor.Solution[j];
                }
                Value = bestNeighbor.Value;

                // zmiejszenie tabu i dodanie do tabu
                DecreaseTabu();
                AddToTabu(bestNeighbor.Number1, bestNeighbor.Number2);
            }
            else
            {
                DecreaseTabu();
            }


        }

        private void DecreaseTabu()
        {
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    if (Tabu[i, j] - 1 > -1) Tabu[i, j]--;
                }
            }
        }

        private void AddToTabu(int number1, int number2)
        {
            Tabu[number1, number2] = TabuValue;
            Tabu[number2, number1] = TabuValue;
        }

        private void generateNewSolution(int[] newSolution, int number1, int number2)
        {
            for (int i = 0; i < Size+1; i++)
            {
                newSolution[i] = Solution[i];
            }

            int temp = newSolution[number1];
            newSolution[number1] = newSolution[number2];
            newSolution[number2] = temp;
        }

        private bool CheckInTabu(int number1, int number2)
        {
            if(Tabu[number1, number2] > 0) return false;
            return true;
        }

        private void InitRandomSolution()
        {
            List<int> list = new List<int>();
            for (int i = 1; i < Size; i++)
            {
                list.Add(i);
            }

            Solution[0] = 0;
            Solution[Size] = 0;

            for (int i = 1; i < Size; i++)
            {
                int selected = random.Next(0, list.Count);
                int selectedValue = list[selected];

                Solution[i] = selectedValue;
                list.Remove(selectedValue);
            }

            Value = getValue(Solution);
        }



        public void ShowResult(int[] solution)
        {
            Console.WriteLine("Rezultat: \n");
            for (int i = 0; i < Size; i++)
            {
                Console.Write(solution[i] + " --> ");
            }
            Console.Write(0);

            Console.WriteLine("\nKoszt: " + getValue(solution));
            Console.ReadKey();
        }

        private int getValue(int[] solution)
        {
            int result = 0;
            for (int i = 0; i < Size; i++)
            {
                result += Matrix[solution[i], solution[i + 1]];
            }
            return result;
        }
    }

    class Neighbor
    {
        public int Number1 { get; set; }
        public int Number2 { get; set; }
        public int[] Solution { get; set; }
        public int Value { get; set; }

        public Neighbor(int size)
        {
            Solution = new int[size + 1];
            Value = Int32.MaxValue;
        }

        public void ChangeBestNeighbor(int number1, int number2, int[] solution, int value)
        {
            Number1 = number1;
            Number2 = number2;
            Value = value;

            for (int i = 0; i < solution.Length; i++)
            {
                Solution[i] = solution[i];
            }
        }
    }
}
